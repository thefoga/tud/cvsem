\section{Preliminaries}
\label{sec:pre}

This section introduces all the necessary theory to understand paper contributions, in particular it introduces the usage of ILP to solve tracking-by-assignment problems.

\subsection{ILP}
\label{subsec:ilp}

% https://www.hni.uni-paderborn.de/fileadmin/Fachgruppen/Algorithmen/Lehre/Vorlesungsarchiv/SS2017/Online/Lecture_6.pdf

Integer linear programming problems, aka ILP, are optimization or feasibility programs where some of the variables are restricted to be $\in \mathbb {Z}$. The \emph{standard form} of such \emph{programs} is defined to be

$$
\begin{aligned}
    \text{maximize } &\mathbf{c} ^{\mathrm {T}} \mathbf {x} \\
    \text{subject to } &A\mathbf {x} + \mathbf {s} = \mathbf {b},\\
    &\mathbf {s} \geq \mathbf {0},\\
    &\mathbf {x} \in \mathbb {Z}_{\ge 0} ^{n}
\end{aligned}
$$

where

\begin{itemize}
    \item $\mathbf{c} ^{\mathrm {T}} \mathbf {x}$ is called \emph{objective function} or \emph{cost}
    \item $\mathbf {c}, \mathbf {b}$ are vectors $\in \mathbb {Z} ^{n}$
    \item $A$ is a matrix, where all entries are $\in \mathbb {Z}$
\end{itemize}

See \citet{ilps} for a more detailed description. The \emph{dual} of a ILP is defined to be (in \emph{standard form})

$$
\begin{aligned}
    \text{minimize } &\mathbf{b} ^{\mathrm {T}} \mathbf {y} \\
    \text{subject to } & A^{\mathrm {T}} \mathbf {y} + \mathbf {s} = \mathbf {c},\\
    &\mathbf {s} \geq \mathbf {0},\\
    &\mathbf {y} \in \mathbb {Z}_{\ge 0} ^{n}
\end{aligned}
$$

\ie{} there is a dual variable for each one of the \emph{primal} constraints.

\begin{fact}[Duality]
\label{fact:Duality}
Creating the dual of a dual yields the primal.
\end{fact}

\begin{exm}[Obtain bounds from the dual]
\label{exm:bounds_dual}
The bounds given by the primal refer to the value of a solution that satisfies the constraints, while the solution of a dual may not always be feasible until the last step of the optimization algorithm. By dualizing the primal, we can express the objective function in terms of the constraints, thereby obtaining a lower bound (upper, in case of maximization problems) of the solution. Take for instance the primal

$$
\begin{aligned}
    \text{minimize } & 7x + 3y \\
    \text{subject to } & x + y \ge 6,\\
    &3x + y \ge 12
\end{aligned}
$$

whose lower bound clearly stems from its constraints:

$$
\begin{aligned}
7x + 3y &= (x + y) + 3 (3x + y)\\
&\ge 6 + 3(12) = 42
\end{aligned}
$$

\end{exm}

\begin{theorem}[Strong Duality]
\label{thm:Strong_Duality}
Given an optimal solution of a primal and an optimal solution of its relative dual, the cost is the same.
\end{theorem}

\begin{fact}[NP-completeness]
\label{fact:np}
ILPs are NP-complete (see \citet{ilp_np}) and thus must be solved via suitable optimization methods.
\end{fact}
\vspace{1cm}

\subsection{Lagrangian decomposition}
\label{subsec:Lagrangian_way}

\begin{defi}[Integer relaxation]
\label{defi:int_relax}
\emph{Integer relaxation} is the process of removing the integrality constraint of variables; linear programming relaxation is a traditional technique for designing approximation algorithms for hard optimization problems.
\end{defi}

\begin{exm}[Find a solution to the original problem from a relaxation]
\label{exm:sol_by_relaxing}
Take the weighted variant set-cover problem (see \citet{Vazirani_algos} for an introduction), where we try to find the smallest sub-collection of sets whose union equals the universe, and each set has a weight associated. It's NP-complete, and clearly it can be solved by assigning an integer (in this case boolean) value to each set, depending on it being part of the chose sub-collection or not. In particular, given an input $U, S_1, S_2, \ldots S_n$ the ILP formulation of such set-cover can be written as

$$
\begin{aligned}
    \text{minimize } & \sum w_i \cdot x_i \\
    \text{subject to } & \sum_{i \mid u \in S_i} x_i \ge 1 \quad \forall u \in U,\\
    & x_i \in \{0, 1\}\\
\end{aligned}
$$

However we can \emph{relax} the integrality constrains (it's as if we are spreading the cost of having each element $u \in U$ to all the sets containing it) expressing the problem into its \emph{fractional cost} version:

$$
\begin{aligned}
    \text{minimize } & \sum w_i \cdot x_i \\
    \text{subject to } & \sum_{i \mid u \in S_i} x_i \ge 1 \quad \forall u \in U,\\
    & x_i \in [0, 1]\\
\end{aligned}
$$
\end{exm}

\begin{remark}[Obtain bounds from the relaxation]
\label{remark:bounds_relax}
Note that the optimal cost of a fractional solution need not equal the cost of its integral one. In fact the cost of a fractional solution is always better or equal to the cost of the integer solution.\\
The process of turning a fractional solution into a integral one is called \emph{rounding}, and one of the traditional approach to rounding is the \emph{randomized rounding}
\end{remark}

\begin{defi}[Lagrangian relaxation]
\label{defi:lag_relax}
The \emph{Lagrangian relaxation} is the process of relaxing constraints and penalizing violations of those constraints in the objective function using coefficients called \emph{multipliers}: the constraints are chosen such that the problem without the constraints (\ie{} \emph{the subproblem}) is much easier to solve than the original problem.
\end{defi}

Often, the solution of the Lagrange relaxation is used to derive feasible solutions for the original problem. By also solving the dual problem, tighter bounds can be obtained.

\begin{defi}[Lagrange decomposition]
\label{defi:lag_dec}
The \emph{Lagrange decomposition} (introduced by \citet{guignard1987lagrangeana}, \citet{guignard1987lagrangeanb} and studied by \citet{a_new_Lagrangean_relaxation}, \citet{guignard2003lagrangean}, \citet{sontag2011introduction}) is a specific form of Lagrangian relaxation in which some of the variables have been duplicated (and the constraint requiring the new variables $x_1 = x_2$ added).
\end{defi}

\subsection{BCA methods}
\label{subsec:bca_methods}

Block-coordinate ascent methods (equivalent to the more common block-coordinate descent methods) are iterative gradient methods where just a subset of the coordinates is updated at each step. This class of methods are often applied to discrete energy minimization problems (as in \citet{kolmogorov2006convergent}, \citet{kolmogorov2015new}, \citet{savchynskyy2019discrete}, \citet{Shekhovtsov-DMM-16}, \citet{tourani2018mplp} prove).

\subsection{Tracking}
\label{subsec:viz_tracking}

In computer vision

\begin{itemize}
    \item \emph{segmentation} is the process of partitioning a digital image into multiple segments, representing onthologically different elements of the reality into consideration. The outcome can be seen as a graph $\SG=(\SV)$ (with no edges) where $\SV$ is the set of segmented instances
    \item the \emph{tracking} problem "consists of segmenting images obtained over $T$ time steps and matching the segments in consecutive images to each other" (\citet{haller2020primaldual}). The solution of this type of problems, is thus a spatio-temporal hyper-graph $\SG=(\SV, \SE)$ where $\SV$ is the set of segmented instances (in all time frames) and $\SE$ is the set connecting two instances of the same entity at consecutive time frames
\end{itemize}

Historically the approaches (and the problems considered) to solve the tracking problem are classified as either "by model evolution" or "by assignment".

\subsubsection{Tracking by model evolution}
\label{subsubsec:tracking_by_model_evolution}

In tracking-by-model-evolution, "objects are detected in the first frame and a model of their properties, \eg{} shape and position, is obtained. This model is
then updated \emph{greedily} for pairs of neighboring frames" (\citet{haller2020primaldual}). Recently neural-network-based solvers (like LSTMs as in \citet{arbelle2018microscopy}) have been successful when vast amounts of annotated training data are available.

\subsubsection{Tracking by assignment}
\label{subsubsec:tracking_by_ass}

On the other hand, tracking-by-assignment "first segments potential object candidates in all time frames" (\citet{haller2020primaldual}) reducing the problem to a combinatorial problem. Historically tracking-by-assignment is categorised

\begin{itemize}
    \item by segmentation hypothesis per entity: allowing \emph{multiple} hypothesis per entity correspond to segmenting the object into many (overlapping) parts and yields better user-driven results
    \item by approach to solution: \emph{local} approaches attempt to overcome scalability issues by solving smaller local sub-problems and building the overall solution by \emph{bottom-up}, whereas \emph{global} approaches treat the spatio-temporal problem as a whole
\end{itemize}

\subsection{Traditional way of solving tracking-by-assignment}
\label{subsec:std_way}

Since \citet{haller2020primaldual} shows an innovative way of solving tracking-by-assignment that builds on existing methods, it's helpful to describe the traditional way and notation. The literature regarding the solution of the tracking-by-assignment problem started developing with the seminar work of \citet{jug2014optimal}, \citet{jug2014tracking}, \citet{kausler2012discrete}, \citet{lou2011deltr}, \citet{schiegg2013conservation} and \citet{schiegg2014graphical}: the terminology and the way to arrive to a solution to the problem shall be thus defined the \emph{Jug-Schiegg} method.\\  % don't call it the "standard way". Invent a speaking name instead!
There are slightly different ways of looking at the problem (\eg{} in \citet{kausler_13_tracking}) but the core idea of the \emph{Jug-Schiegg} method is based on the spatio-temporal hyper-graph representation (see Figure 1 in \citet{haller2020primaldual}) $\hat\SG=(\hat\SV, \hat\SE)$ where

\begin{itemize}
    \item the set of all (binary-valued, $x_v \in \{0, 1\}$) nodes can be seen as a union of disjoint subsets at different times: $\hat\SV = \bigcup_{t=1}^{T}\hat\SV^t$. Moreover, nodes at the same timestep $t$ can be seen as the union of 2 disjoint subsets: $\hat\SV_{\text{det}}^t$ (the detection, or \emph{segmentation hypothesis}) and $\hat\SV_{\text{trans}}^t$ (the transition from the current timestep $t$ to the next $t+1$)
    \item the edges $\hat\SE \subseteq 2 ^ {\hat\SV}$ correspond to coupling constraints between the respective nodes
\end{itemize}

Under this notation

\begin{itemize}
    \item a possible move is a triple $(u,v,w) \in \hat\SE^t_\Smove$ from time step $t$ to $t+1$ connecting nodes $u\in\hat\SV^t_\Sdet$ and $v\in\hat\SV^t_\Sdet$ via transition node $w\in\hat\SV^t_\Strans$ (or simplier $\SHatEdgeTrans uvw$)
    \item a possible division is a quadruple $(u,v,v',w)\in \hat\SE^t_\Sdiv$ denoted by $\SHatEdgeDiv{u}{v}{v'}{w}$
    \item $\forall v \in \hat\SV_\Sdet$ let's denote by $\hat\SIn(v)$ the set of \emph{incoming} transitions
    \item likewise $\forall u\in\SV_\Sdet \hat\SOut(u)$ is the set of \emph{outgoing} transitions
\end{itemize}

Furthermore, by setting the following constraints

\begin{itemize}
    \item \emph{coupling}: $\forall\, \SHatEdgeTrans uvw \in \hat\SE_{\Smove}: \quad x_w \le x_u \;\wedge\; x_w \le x_v$ ensuring that \emph{deactivating} (\ie{} $x_u = 0$ or (not exclusive) $x_v = 0$) deactivates also its relative move ($x_w = 0$)
    \item \emph{division}: $\forall\, \SHatEdgeDiv{u}{v}{v'}{w} \in \hat\SE_\Sdiv: x_w \!\le\! x_u \;\wedge\; x_w \!\le\! x_v \;\wedge\; x_w \!\le\! x_{v'}$
    \item \emph{uniqueness}: $\forall\, v\in\hat\SV_\Sdet\colon \sum_{w\in\hat\SIn(v)}\!\! x_w\le 1 \quad\text{ and }  \sum_{w\in\hat\SOut(v)}\!\!\! x_w\le 1$ meaning that at most, there is $1$ possible transition from (to) the previous (next) timestep
    \item $\forall\, c\in\hat\SE_\Sconf\colon \sum_{v\in c} x_v \le 1$ handling \emph{conflicting} segmentation hypothesis
\end{itemize}

and by associating a \emph{cost} $\theta_v \in \mathbb{R} \forall x_v, v \in \hat\SV$ (based on image data and geometric prior assumptions)

\begin{equation}
\label{equ:standard_ILP}
    \min_{\SVar\in\hat\SX} \; \SSP{\SCost}{x}
\end{equation}

is the objective function of the (binary-valued) ILP.