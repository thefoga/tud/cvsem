\section{Conceptual contributions}
\label{sec:conceptual_contributions}

Since $\hat\SG$ built in \prettyref{subsec:std_way} is used to solve \prettyref{equ:standard_ILP}, the representation of $\hat\SG$ matters a lot and that's where \citet{haller2020primaldual}'s efforts are first put into. Secondly, since the relative ILP is not solvable (efficiently) by traditional methods, \citet{haller2020primaldual} designs a new optimization algorithm (based on \prettyref{subsec:bca_methods}) and a way to find a first primal heuristics. This section summarizes these results.

\subsection{Decomposable representation}
\label{subsec:new_way}

By Lagrangian-decomposing (see \prettyref{subsec:Lagrangian_way}) $\hat\SG$ one arrives at $\SG=(\SV, \SE)$ where

\begin{itemize}
    \item all binary variables have been duplicated and regrouped: each \emph{group} corresponds to a new graph node (see Figure 1 in \citet{haller2020primaldual}). $\SV$ contains only 2 types of nodes: \emph{detection} and \emph{conflict} nodes: $\SV=\SV_\Sdet\cup\SV_\Sconf$, where $\SV_\Sdet := \hat\SV_\Sdet$ and $\SV_\Sconf := \hat\SE_\Sconf$.
    \item $\SE$ contains either a \emph{transition} or a \emph{conflict}: $\SE = \SE_\Smove \cup \SE_\Sdiv \cup \SE_\Sconf$, where
    \begin{itemize}
        \item $\SE_{\Smove}:=\hat\SE_{\Smove} \subseteq(\SV_\Sdet)^2$ correspond to moves, and $\SE_{\Sdiv}:=\hat\SE_{\Sdiv} \subseteq (\SV_\Sdet)^3$ correspond to divisions
        \item $\SE_\Sconf \subseteq \SV_\Sdet \times \SV_\Sconf$ are the \emph{conflict edges}: $\SEdgeConf uc$ is active between any detection node $u \in \SV_\Sdet$ and conflict node $c \in \SV_\Sconf \iff$ $\hat u \in \hat c$ for the corresponding $\hat u\in\hat\SV_\Sdet$ and $\hat c\in\hat\SE_{\Sconf}$.  % todo not needed to define conf(u)
    \end{itemize}
    \item all transition variables have been duplicated (tripled for divisions) and their copies have been assigned to the corresponding detection nodes
    \item $\forall u\in\SV_\Sdet \SOut(u) = \hat\SOut(u), \SIn(u) = \hat\SIn(u)$
\end{itemize}

this means that $u \in \SV_\Sdet$ is \emph{active} because of its set of states

\begin{equation}
\label{equ:detection_factor}
    \SX_u \!=\! \left\{\!
        \left(
        \begin{aligned}
          x_\Sdet &\in \{0,1\}\\[-3pt]
          x_\SIn  &\in \{0,1\}^{|\SIn(u)|}\\
          x_\SOut &\in \{0,1\}^{|\SOut(u)|}
        \end{aligned}
        \right)
      \middle|\!
        \begin{array}{l}
          \SSP{\SOne}{x_\Sin} \leq x_\Sdet,\\
          \SSP{\SOne}{x_\Sout} \leq x_\Sdet
        \end{array}
    \!\!\right\}
\end{equation}

Note that the conditions on the \textit{RHS} of \prettyref{equ:detection_factor} express just the number of \emph{activ}ated incoming and outgoing transitions. The new \emph{coupling constraints} become

\begin{equation}
\label{equ:new_coupling_cons}
\begin{aligned}
    &\forall\, e = \SEdgeTrans uv \in \SE_\Smove\colon &\SVarFactorOut{u}(e) &= \SVarFactorIn{v}(e), \nonumber\\
    &\forall\, e = \SEdgeDiv uvw \in \SE_\Sdiv\colon &\SVarFactorOut{u}(e) &= \SVarFactorIn{v}(e), \nonumber\\
    &\forall\, e = \SEdgeDiv uvw \in \SE_\Sdiv\colon &\SVarFactorOut{u}(e) &= \SVarFactorIn{w}(e), \nonumber \\
    &\forall\, \SEdgeConf uc \in \SE_\Sconf\colon &\SVarFactorDet{u} &= \SVarFactorConf{c}(u)
\end{aligned}
\end{equation}

which lead to a different (than \prettyref{equ:standard_ILP}) minimization problem:

\begin{equation}
\label{equ:new_min}
    \min_{x \in \SX}
      \biggl[
        E(\theta, x)
        \!:=\!
        \sum_{{u \in \SV_\Sdet}}  \SSP{\SCostFactor u}{\SVarFactor u} +
        \sum_{{c \in \SV_\Sconf}} \SSP{\SCostFactor c}{\SVarFactor c}
      \biggr] .
\end{equation}

\subsection{Dual construction}
\label{subsec:dual_way}

Defining $\SRepa \in \Lambda:=\SR^{|\SE_\Smove|+2|\SE_\Sdiv|+|\SE_\Sconf|}$ the reparametrization vector (\ie{} $\SRepa(e)\in\SR$ is the dual variable constraining $x_{u,\Sin}(e) := x_{v,\Sout}(e) \iff e$ is a move and $\SVarFactorDet{u}=\SVarFactorConf{c}(u) \iff e$ is a conflict, $\theta ^ {\lambda}$ being the \emph{reparametriz}ed costs) and proceeding with the dualization of \prettyref{equ:new_coupling_cons} yields

\begin{flalign*}
    &
    \begin{aligned}
      &\forall\, c\in\SV_\Sconf \,\forall\, u\in c \colon &
      \SCostRepaFactor c(u) &:=
        \SCostFactor c(u) + \SRepa(\SEdgeConf uc), &&
      \\
      &\forall\, u\in\SV_\Sdet \colon &
      \SCostRepaFactorDet u  &:=
        \SCostFactorDet u - \sum_{{e \in \Sconf (u)}} \SRepa(e),
    \end{aligned}
    \\[-0.8em]
    &\forall\, u\in\SV_\Sdet \,\forall\, e \in \SIn(u) \colon
    \\
    &\quad\SCostRepaFactorIn u(e) :=
        \begin{cases}
          \SCostFactorIn u(e) + \SRepa(e), & \!\!\!e \in \SE_\Smove \\
          \SCostFactorIn u(e) + \SRepa_u(e), & \!\!\!e \in \SE_\Sdiv
        \end{cases},
    \\
    &\forall\, u\in\SV_\Sdet \,\forall\, e \in \SOut(u) \colon &
    \\
    &\quad\SCostRepaFactorOut{u}(e) :=
        \begin{cases}
          \SCostFactorOut u(e) \!-\! \SRepa(e),              & \!\!\!e \in \SE_\Smove \\
          \SCostFactorOut u(e) \!-\! \sum\limits_{{v'\in\{v,w\}}} \SRepa_v(e), & \!\!\!e\! =\! \SEdgeDiv uvw \!\in\! \SE_\Sdiv\!
        \end{cases}
\end{flalign*}

\ie{} the (Lagrange) dual problem with objective

\begin{equation}
\label{equ:lagrange_max}
    D(\SRepa) :=
      \sum_{{u \in \SV_\Sdet}}  \; \min_{x_u \in \SX_u} \SSP{\SCostRepaFactor u}{\SVarFactor u} +
      \sum_{{c \in \SV_\Sconf}} \; \min_{x_c \in \SX_c} \SSP{\SCostRepaFactor c}{\SVarFactor c}
    \,.
\end{equation}

which, by construction, $\max_{\SRepa\in\Lambda} D(\SRepa) \le \min_{\SVar\in\SX} E(\SCost, \SVar)$.\\
Note that \prettyref{equ:lagrange_max} is a sum of small-sized minimization problems, which can be solved in linear time. Furthermore, proving that \prettyref{equ:lagrange_max} yields the same value as the natural LP relaxation of \prettyref{equ:new_min} is trivial, since it's just an application of \citet{guignard2003lagrangean}, \citet{guignard1987lagrangeana} and \citet{guignard1987lagrangeanb}.

\subsection{Optimization algorithm}
\label{subsec:optim_algorithm}

\input{sections/algorithm_dual}

Following BCA principles, \citet{haller2020primaldual} devise an optimization algorithm that monotonically-increases \prettyref{equ:lagrange_max}. \prettyref{alg:dual} updates $\SG$ via 4 types of updates across consecutive time steps:

\begin{itemize}
    \item $\SMsgFromFact u(e) := \min\limits_{x\in\SX_u \colon x_\Sdet = 1} \frac{\SSP{\SCost _u}{x}}{|\Sconf (u)|}$ called \emph{conflict updates}: they promote agreement among local minimizers of the conflict nodes and the associated detection nodes
    \item $\SMsgFromConf c(e) := - \SCost _c(u) + \frac{1}{2} \bigl[ \SSP{\SCost _c}{z_c^\star} + \SSP{\SCost _c}{z_c^{\star\star}} \bigr]$ called (dual) \emph{transition updates}, needed to propagate the costs to the next time step
\end{itemize}

It has to be noted that, despite

\begin{itemize}
    \item \prettyref{alg:dual} monotonically increases the objective function
    \item and the value of the objective function is bounded by the dual
    \item thus converging to an final (optimal) cost
\end{itemize}

the final (optimal) cost is not necessary a global optimum! This is due to a well-known property of block-coordinate-ascent methods when applied to non-smooth functions: they may get stuck in local optima.

\subsection{Heuristics computation}
\label{subsec:heuristics}

\input{sections/algorithm_primal}

Since \prettyref{equ:lagrange_max} runs on the dual $\theta ^ {\lambda}$, it is not guaranteed to find a \emph{feasible} solution also for the primal: even optimal solutions to node sub-problems can be inconsistent. The proposed \prettyref{alg:primal}

\begin{itemize}
    \item runs forward and backward in time, keeping the best assignment
    \item estimates the primal solution in each time frame (with a off-the-shelf solver)
    \item first solves the conflicts in current time step $t$, then manages inter-frame edges: the task is $NP$-hard but \citet{haller2020primaldual} assures that the instances are small and thus can be solved in very little time
\end{itemize}
